#!/bin/sh

if [ -n "$ZIGBEE2MQTT_CONFIG_B64" ]
then
  CONFIG=$(echo "$ZIGBEE2MQTT_CONFIG_B64" | base64 -d)
  if [ $? == 0 ]
  then
    echo "Applying new configuration from env:"
    echo "$CONFIG" | tee /data/configuration.yaml
  else
    echo "ZIGBEE2MQTT_CONFIG_B64 is invalid base64; not applying" >&2
  fi
else
  echo "ZIGBEE2MQTT_CONFIG_B64 is unset; not touching configuration"
fi

export ZIGBEE2MQTT_DATA=/data
exec $1
